//LEFT MENU
var leftMenu = document.querySelector(".left-menu");
//WHITE BACKGROUND
var white = document.querySelector(".white-background");
//ICONS TEXT
var texticons = document.querySelectorAll(".icon-hover");
//RIGHT SIDE "BUTTON"
var rightbutton = document.querySelector(".righ-side-information__side-info__retangle");
//RIGHT SIDE COLUMN
var column = document.querySelector(".righ-side-information");
//DOUBLE ARROWS
var arrows = document.querySelector(".left-menu__header__logo__retangle");
//MIDDLE DIV
var middle = document.querySelector(".middle-div ");
//MIDDLE GROUP 
var middle_group = document.querySelector(".middle-div__group-middle__static-elements");

//Left Menu

//On Mouse Hover //Left Menu
leftMenu.addEventListener("mouseover", function () {


    this.classList.add('left-menu-change');
    white.classList.add('content-change');
    arrows.classList.add('left-arrows-style-change');

    texticons.forEach((element) => (element.style.display = "inline-grid"));


});

//On Mouse Out //Left Menu

leftMenu.addEventListener("mouseout", function () {

    this.classList.remove('left-menu-change');
    white.classList.remove('content-change');
    arrows.classList.remove('left-arrows-style-change');

    texticons.forEach((element) => (element.style.display = "none"));

}); 

//RIGHT MENU

//MOUSE HOVER E MOUSE OUT OF BUTTON
rightbutton.addEventListener("mouseover", function () {
    
    rightbutton.classList.add('right-button');

});

rightbutton.addEventListener("mouseout", function () {
    
    rightbutton.classList.remove('right-button');
});


var layout = document.querySelector('.dashboard');
rightbutton.addEventListener("click", function () {
    column.classList.toggle('hide-menu');

    if(!column.classList.contains('hide-menu')){
        layout.classList.add('sidebar-is-open')
        middle.classList.add('is_open');
    }else{
        layout.classList.remove('sidebar-is-open')
        middle.classList.remove('is_open');
    }
})


//------------------------------------------        Codigo Antigo         ------------------------------------------// 

/*rightbutton.addEventListener("click", function () {

    if (!column.classList.contains("hide-menu")) {

        column.classList.add("hide-menu");

        

        var aux = visualViewport.width - "300" + "px";

        middle_group.style.width = aux;

        
        middle.style.width = "100%";

    } else {

        column.classList.remove("hide-menu");
    
        middle.style.width = Variable ;

        middle_group.style.width = aux;

    }
});*/